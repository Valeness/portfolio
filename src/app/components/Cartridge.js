import Entity from "./Entity";
import Globals from "./Globals";

class Cartridge extends Entity {
    constructor() {
        super();
        this.gone_above = false;
        this.dropped = false;
        this.move_id = 0;
        this.cartridge = 'about';
    }

    setElement(options) {
        const self = this;

        self.cartridge = options.cartridge;

        let cartridge_class = 'cartridge-' + self.cartridge;
        let cartridge = document.createElement('div');
        cartridge.classList.add('cartridge');
        cartridge.classList.add('being-dragged');
        cartridge.innerHTML = '<div class="cartridge-image being-dragged"></div>'
        cartridge.setAttribute('style', 'margin: 0; position: fixed;')
        cartridge.querySelector('.cartridge-image').classList.add(cartridge_class);
        self.$el = cartridge
    };

    renderDrop(start_loc, target_center, options, done_callback) {
        const self = this;

        if(options === undefined) {
            options = {};
        }

        if(options.pause_on !== undefined) {
            if(self[options.pause_on] === true) {

                return false;
            }
        }

        let c_loc = start_loc
        let current_loc = self.getPos();
        const t = self.getTime();
        self.setTime(t + self.t_step);

        // Generate a bezier curve between the cartridge and the slot
        let p0 = {x : c_loc.center_x, y : c_loc.center_y * -1};
        let p1 = {x : target_center.x , y : c_loc.center_y};
        let p2 = {x : target_center.x, y : target_center.y * -1};

        let time_delta_x = 1 - t;
        let p0_time_delta_x = time_delta_x * p0.x;
        let p1_time_x = t * p1.x;

        let p1_time_delta_x = time_delta_x * p1.x;
        let p2_time_x = t * p2.x;


        let phrase_1_x = time_delta_x *(p0_time_delta_x + p1_time_x);
        let phrase_2_x = t * (p1_time_delta_x + p2_time_x)

        let new_x = phrase_1_x + phrase_2_x;

        let time_delta_y = 1 - t;
        let p0_time_delta_y = time_delta_y * p0.y;
        let p1_time_y = t * p1.y;

        let p1_time_delta_y = time_delta_y * p1.y;
        let p2_time_y = t * p2.y;


        let phrase_1_y = time_delta_y *(p0_time_delta_y + p1_time_y);
        let phrase_2_y = t * (p1_time_delta_y + p2_time_y)


        let new_y = (phrase_1_y + phrase_2_y);

        let new_center_x = new_x - (start_loc.width / 2);
        let new_center_y = new_y + (start_loc.height / 2);

        let bottom_edge = current_loc.y + current_loc.height;
        let slot_bottom_edge = target_center.y + target_center.height;

        let width = current_loc.width;
        let height = current_loc.height;

        if(bottom_edge + 30 >= slot_bottom_edge && self.gone_above && (options.slot === undefined || (options.slot === true))) {
            height = height - ((bottom_edge - slot_bottom_edge) + 15);
        } else if(bottom_edge + 30 < slot_bottom_edge) {
            self.gone_above = true;
        }

        self.setPos(new_center_x, new_center_y * -1);
        self.setDimensions(width, height);
        self.render();


        // return false;
        if(t < 1) {
            // if(t > 0.90) {
            //     self.t_step = 0.03;
            // }
            self.animation_id = requestAnimationFrame(self.renderDrop.bind(this, start_loc, target_center, options, done_callback))
        } else {
            self.setTime(0);
            window.cancelAnimationFrame(self.animation_id);
            if(done_callback !== undefined) {
                done_callback()
            }
        }
    };

    handleMouseMove(mouse_location) {
        super.handleMouseMove();
        const self = this;

        if(self.dropped) {
            return false;
        }

        let x = mouse_location.x;
        let y = mouse_location.y;

        let c_loc = self.getPos();
        let delta_x = (c_loc.x + 75) - x;

        let $cartridge_image = self.$el.querySelector('.cartridge-image');

        if(delta_x > 0 && !$cartridge_image.classList.contains('turn-left')) {
            $cartridge_image.classList.remove('turn-right')
            $cartridge_image.classList.add('turn-left')
        } else if(delta_x < 0 && !$cartridge_image.classList.contains('turn-right')) {
            $cartridge_image.classList.remove('turn-left')
            $cartridge_image.classList.add('turn-right')
        }

        if(self.move_id > 0) {
            $cartridge_image.classList.remove('stop')
            window.clearTimeout(self.move_id)
        }

        self.move_id = window.setTimeout(function() {
            $cartridge_image.classList.remove('turn-left')
            $cartridge_image.classList.remove('turn-right')

        }, 20)
        self.setPos((x - 75), (y - 25));
        self.render();

    };

    handleMouseUp() {
        super.handleMouseUp();
        const self = this;

        if(self.dropped === true) {
            return false;
        }

        self.dropped = true;

        const $slot = document.querySelector('.cartridge-slot')

        // Get center-point of cartridge
        let cartridge_loc = self.getPos()
        let cartridge_height = cartridge_loc.height;

        $slot.classList.add('cartridge-cover-top')
        let cartridge_centerpoint = {
            x : (cartridge_loc.x + (cartridge_loc.width / 2)),
            y: (cartridge_loc.y + (cartridge_loc.height / 2))
        }

        // Get center-point of cartridge slot
        let slot_loc = Globals.getPos($slot)
        let slot_centerpoint = {
            x : (slot_loc.x + (slot_loc.width / 2)),
            y: (slot_loc.y + (slot_loc.height / 2)),
            width : slot_loc.width,
            height : slot_loc.height,
        }

        let delta_x = cartridge_centerpoint.x - slot_centerpoint.x;
        let delta_y = cartridge_centerpoint.y - slot_centerpoint.y;

        let x_direction = delta_x < 0 ? -1 : 1;
        let y_direction = delta_y < 0 ? -1 : 1;
        // console.log(x_direction)

        let step = delta_x / 200;
        self.current_step = 0;

        let m = delta_y / delta_x
        self.line_data = {
            m : m,
            delta_x : delta_x,
            delta_y : delta_y,
            center_x : cartridge_centerpoint.x,
            step : step
        }

        // Start the animation loop
        self.renderDrop(cartridge_loc, slot_centerpoint, {
            pause_on : 'slot_pause'
        }, (c) => {
            let $template = document.querySelector('[data-id="'+self.cartridge+'"]');
            let html = $template.innerHTML;
            self.setContent(html);

            if(self.cartridge === 'game') {
                garden.init();
                garden.loadWorld('space');
            }

        });

        // On Drop, we need to see if any other entities are also being dropped or are in the slot. Stop their drop
        // and/or remove them from the slot. We will be tapping into global state here, it's my code, fucking sue me lol
        for(let a in app.entities) {
            let entity = app.entities[a];
            if(entity.dropped && entity.index !== self.index) {
                // Get Entity2 Original Location
                if(entity.back_to_home !== true) {
                    entity.goToHomePoint();
                }
            }
        }

    };

    setContent(content) {
        const self = this;
        document.querySelector('.text-body').innerHTML = content;
    };

    goToHomePoint() {
        const self = this;

        self.setContent('<h1>Loading...</h1>');

        self.slot_pause = true;
        self.back_to_home = true;
        self.setDimensions(self.original_pos.width, self.original_pos.height);
        self.render();

        self.gone_above = false;

        // Get Original Location
        let original_location = self.original_pos;

        // Find original location centerpoint
        let original_centerpoint = {
            x : original_location.x + (original_location.width / 2),
            y : original_location.y + (original_location.height / 2)
        }

        self.setTime(0)

        // Drop to location
        self.renderDrop(self.getPos(), original_centerpoint, {}, () => {
            self.remove();
            delete app.entities[self.index];

        });
    };

    handleMouseDown() {
        super.handleMouseDown();
    };

    inSlot() {
        const self = this;

        return true;

        const $slot = document.querySelector('.cartridge-slot')

        let clicked_loc = self.getPos();
        let slot_loc = Globals.getPos($slot)

        let padding = 100;

        let bottom_edge = clicked_loc.y + clicked_loc.height
        let left_edge = clicked_loc.x;
        let right_edge = clicked_loc.x + clicked_loc.width

        return (
            left_edge > (slot_loc.x - padding) &&
            right_edge < (slot_loc.x + slot_loc.width + padding) &&
            bottom_edge < (slot_loc.y + slot_loc.height) &&
            bottom_edge > (slot_loc.y - slot_loc.height)
        );
    };
}

export default Cartridge;