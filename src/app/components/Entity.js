import Globals from "./Globals";

class Entity {

    constructor(x, y, width, height) {
        const self = this;

        self.on_dom = false;

        self.t_step = 0.01;
        self.t = 0;
        self.index = 0;
        self.animation_id = 0;
        self.pause = false;

        self.pos = {
            x : x || 0,
            y : y || 0,
            width : width || 0,
            height : height || 0,
        }
    };

    setTime(t) {
        const self = this;
        self.t = t;
    };

    getTime() {
        const self = this;
        return self.t;
    };

    setElement(options) {
        const self = this;
        self.$el = document.createElement('div');
    };

    getPos() {
        const self = this;
        return Globals.getPos(self.$el);
    };

    setPos(x, y) {
        const self = this;

        self.pos.x = x;
        self.pos.y = y;
    };

    setDimensions(w, h) {
        const self = this;

        self.pos.width = w;
        self.pos.height = h;
    };

    remove() {
        const self = this;
        self.$el.remove();
    };

    render() {
        const self = this;

        if(!self.on_dom) {
            document.body.appendChild(self.$el);
            self.on_dom = true;
        }

        self.$el.style.left = self.pos.x + 'px';
        self.$el.style.top = self.pos.y + 'px';

        if(self.pos.width && self.pos.height) {
            self.$el.style.width = self.pos.width + 'px';
            self.$el.style.height = self.pos.height + 'px';
        }



    };

    handleMouseUp() {

    };

    handleMouseDown() {

    };

    handleMouseMove(mouse_location) {

    };

}

export default Entity;