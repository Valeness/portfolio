class Globals {
    static getPos($el) {
        let bounding_rect = $el.getBoundingClientRect();
        // let style = window.getComputedStyle(self.$el);
        // let margin_top = parseInt(style.marginTop.replace('px', ''));
        // let margin_left = parseInt(style.marginLeft.replace('px', ''));

        let x = bounding_rect.left;
        let y = bounding_rect.top;
        let width = bounding_rect.width;
        let height = bounding_rect.height;

        return {
            x : x,
            y : y,
            width: width,
            height: height,
            center_x : x + (width / 2),
            center_y : y + (height / 2),
        };
    }
}

export default Globals;