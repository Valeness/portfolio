import Cartridge from "./components/Cartridge";
import Globals from "./components/Globals";

const app = {

    mousedown_events : [],
    mouseup_events : [],
    mousemove_events : [],

    entities : [],

    init() {
        const self = this;


        self.bindEvent('mousedown', '.cartridge-image', function(e) {

            let $clicked_element = e.target;

            if($clicked_element.classList.contains('being-dragged')) {
                return false;
            }

            let data = $clicked_element.dataset;
            let clicked_location = Globals.getPos($clicked_element);

            // Create the Entity2
            let draggable_cartridge = new Cartridge();

            draggable_cartridge.setElement(data);
            draggable_cartridge.setPos(clicked_location.x, clicked_location.y);
            draggable_cartridge.render();
            draggable_cartridge.original_pos = clicked_location;

            let index = self.entities.push(draggable_cartridge)
            self.entities[index - 1].index = index - 1;

            for(let i in self.entities) {
                let entity = self.entities[i];

                if(entity.handleMouseDown !== undefined) {
                    entity.handleMouseDown({
                        x : e.clientX,
                        y : e.clientY
                    });
                }
            }


        })

        self.bindEvent('mouseup', '.cartridge-image', function() {

            for(let i in self.entities) {
                let entity = self.entities[i];

                if(entity.handleMouseUp !== undefined) {
                    entity.handleMouseUp();
                }
            }


        })

        self.bindEvent('mousemove', '.cartridge', function(e) {

            for(let i in self.entities) {
                let entity = self.entities[i];

                if(entity.handleMouseMove !== undefined) {
                    entity.handleMouseMove({
                        x : e.clientX,
                        y : e.clientY
                    });
                }
            }
        })

        self.bindActions();

        // Create the Entity2
        let draggable_cartridge = new Cartridge();

        const $cart = document.querySelector('[data-cartridge="game"]');
        let cartridge_location = Globals.getPos($cart);

        draggable_cartridge.setElement({
            cartridge : 'game'
        });
        draggable_cartridge.setPos(cartridge_location.x, cartridge_location.y);
        draggable_cartridge.render();
        draggable_cartridge.original_pos = cartridge_location;

        let index = self.entities.push(draggable_cartridge)
        self.entities[index - 1].index = index - 1;
        draggable_cartridge.handleMouseUp();
        console.log('here');
    },

    bindEvent(event_type, selector, method) {
        const self = this;
        self[event_type + '_events'].push({
            selector : selector,
            method : method
        })
    },

    bindActions() {
        const self = this;

        document.addEventListener('mousedown', function (event) {

            for(let i in self.mousedown_events) {
                let e = self.mousedown_events[i];
                if (!event.target.matches(e.selector)) continue;

                event.preventDefault();

                e.method(event)

            }

        }, false);

        document.addEventListener('mouseup', function (event) {

            for(let i in self.mouseup_events) {
                let e = self.mouseup_events[i];
                if (!event.target.matches(e.selector)) continue;

                event.preventDefault();

                e.method(event)

            }

        }, false);

        document.addEventListener('mousemove', function (event) {

            for(let i in self.mousemove_events) {
                let e = self.mousemove_events[i];
                // if (!event.target.matches(e.selector)) continue;

                event.preventDefault();

                e.method(event)

            }

        }, false);
    }


}

window.app = app;

export default {app};