const path = require('path');

module.exports = [
    {
        module: {
            rules: [
                {
                    test: /node_modules[/\\]jsonstream/i,
                    use: 'shebang-loader'
                }
            ]
        },
        entry: './src/game/game.js',
        output: {
            filename: 'game.dist.js',
            path: path.resolve(__dirname, 'public/assets/js'),
        },
    },
    {
        module: {
            rules: [
                {
                    test: /node_modules[/\\]jsonstream/i,
                    use: 'shebang-loader'
                }
            ]
        },
        entry: './src/app/app.js',
        output: {
            filename: 'app.dist.js',
            path: path.resolve(__dirname, 'public/assets/js'),
        },
    },
];
